<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Template 1</title>
        <link rel="stylesheet" href="css/app.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    </head>
    <body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <a class="navbar-brand" href="#">
                    <i class="fa fa-3x fa-500px service-object"></i>
                    {{--<img alt="Brand" src="{{ URL::asset('img/logo/logo.png') }}">--}}
                </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Dienstleistungen</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Geschichte</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Team</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Kontakt</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
@yield('header')
    @yield('content')
    <script src="/js/app.js"></script>
    </body>
</html>
