@extends('app')

@section('header')
    <header>
        <div id="landing-page">

        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div id="quote">
                        <h1>
                            Coming together is a beginning, <br>
                            keeping together is progress, <br>
                            working together is success!<br>
                        </h1>
                    </div>
                    <div id="quote-author">
                        <h2>
                            <i>Henry Ford &ast;1863 &dash; &dagger;1947 </i>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </header>
@stop

@section('content')
    <section id="intro">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Lorem Ipsum</h2>
                <hr>
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                        ut
                        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                        dolores
                        et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                        ut
                        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                        dolores
                        et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                        amet.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="service">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Lorem Ipsum</h2>
                <hr>
                <div id="service-wrapper">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-desktop service-object"></i>
                            <h3>Lorem Ipsum</h3>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-code-fork service-object"></i>
                            <h3>Lorem Ipsum</h3>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-line-chart service-object"></i>
                            <h3>Lorem Ipsum</h3>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-heart-o service-object"></i>
                            <h3>Lorem Ipsum</h3>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                                nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="banner">
        <div class="container no-grutter">
            <div class="row text-center text-lowercase no-grutter" id="banner-box">
                <h2>Lorem ipsum dolor sit amet</h2>
            </div>
        </div>
    </section>
    <section id="contact-short">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x service-object"></i>
                    <p>123-456-6789</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x service-object"></i>
                    <p><a class="text-muted" href="mailto:your-email@your-domain.com">feedback@startbootstrap.com</a></p>
                </div>
            </div>
        </div>
    </section>
@stop